# pieceofdata.de

Animated landing page for pieceofdata.de

## Authors

* **Sascha Voth** - *Initial work* - [sascha-voth](https://gitlab.com/sascha-voth/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details